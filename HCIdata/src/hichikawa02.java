//package test;

import java.util.Random;

public class hichikawa02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new hichikawa02();
	}
	
	public hichikawa02() {
		//11,17,13
		double cd = 430;
		
		double[][] data =
				{{150, 90, 390, 330, 100, 8.8},
				{150, 330, 390, 330, 100, 16.1},
				{270, 210, 390, 330, 100, 33.2},
				{270, 450, 390, 330, 100, 37.1},
				{390, 90, 390, 330, 100, 11.3},
				{390, 330, 390, 330, 100, 435},
				{510, 210, 390, 330, 100, 36.7},
				{510, 450, 390, 330, 100, 40.5},
				{630, 90, 390, 330, 100, 8.9},
				{630, 330, 390, 330, 100, 17.6},
				{150, 90, 330, 270, 100, 18.3},
				{150, 330, 330, 270, 100, 30.7},
				{270, 210, 330, 270, 100, 172},
				{270, 450, 330, 270, 100, 27},
				{390, 90, 330, 270, 100, 22.9},
				{390, 330, 330, 270, 100, 122},
				{510, 210, 330, 270, 100, 25.4},
				{510, 450, 330, 270, 100, 12.5},
				{630, 90, 330, 270, 100, 8.2},
				{630, 330, 330, 270, 100, 9.3},
				{150, 90, 270, 330, 100, 15.4},
				{150, 330, 270, 330, 100, 96.4},
				{270, 210, 270, 330, 100, 83.7},
				{270, 450, 270, 330, 100, 95.2},
				{390, 90, 270, 330, 100, 10.1},
				{390, 330, 270, 330, 100, 78.5},
				{510, 210, 270, 330, 100, 11.2},
				{510, 450, 270, 330, 100, 11.4},
				{630, 90, 270, 330, 100, 5.6},
				{630, 330, 270, 330, 100, 7.1}};
//		double[][] data =
//				{{150, 90, 390, 330, 195, 29.8},
//				{150, 330, 390, 330, 195, 43.1},
//				{270, 210, 390, 330, 195, 92},
//				{270, 450, 390, 330, 195, 39.5},
//				{390, 90, 390, 330, 195, 42.7},
//				{390, 330, 390, 330, 195, 89},
//				{510, 210, 390, 330, 195, 37.8},
//				{510, 450, 390, 330, 195, 21.9},
//				{630, 90, 390, 330, 195, 14.3},
//				{630, 330, 390, 330, 195, 16.4},
//				{150, 90, 330, 270, 195, 29.8},
//				{150, 330, 330, 270, 195, 43.1},
//				{270, 210, 330, 270, 195, 92},
//				{270, 450, 330, 270, 195, 39.5},
//				{390, 90, 330, 270, 195, 42.7},
//				{390, 330, 330, 270, 195, 89},
//				{510, 210, 330, 270, 195, 37.8},
//				{510, 450, 330, 270, 195, 21.9},
//				{630, 90, 330, 270, 195, 14.3},
//				{630, 330, 330, 270, 195, 16.4},
//				{150, 90, 270, 330, 195, 25},
//				{150, 330, 270, 330, 195, 80},
//				{270, 210, 270, 330, 195, 73.1},
//				{270, 450, 270, 330, 195, 79.3},
//				{390, 90, 270, 330, 195, 19.3},
//				{390, 330, 270, 330, 195, 72.3},
//				{510, 210, 270, 330, 195, 20},
//				{510, 450, 270, 330, 195, 21.3},
//				{630, 90, 270, 330, 195, 9.3},
//				{630, 330, 270, 330, 195, 12.3}};

		
		
		
		
		Random rand = new Random();
		System.out.println("i\t実距離\t推定誤差");
		
		for (int k = 0; k < data.length; k++) {
			double minMoku = Double.MAX_VALUE;
			double minX = Double.MAX_VALUE;
			double minY = Double.MAX_VALUE;
			double minH = Double.MAX_VALUE;
			for (int i = 0; i < 100000; i++) {
//				double x = rand.nextDouble() * 60 * 12;
//				double y = rand.nextDouble() * 60 * 10;
				double x = data[k][2];
				double y = data[k][3];
				double h = rand.nextDouble() * 500;
//				double[] cossita = new double[3];
//				for (int j = 0; j < cossita.length; j++) {
//					cossita[j] = h / (Math.sqrt(Math.pow(h, 2) + Math.pow((x-area[j][0]), 2) + Math.pow((y-area[j][1]), 2)));
//				}
				double cossita = h / (Math.sqrt(Math.pow(h, 2) + Math.pow((x-data[k][0]), 2) + Math.pow((y-data[k][1]), 2)));
//				double dist = Math.sqrt(Math.pow(x-trueX, 2) + Math.pow(y-trueY, 2) + Math.pow(h-trueH, 2));
				
//				double tmps[] = new double[3];
//				for (int j = 0; j < tmps.length; j++) {
//					double d2jo = Math.pow(h, 2) + Math.pow((x-area[j][0]), 2) + Math.pow((y-area[j][1]), 2);
//					tmps[j] = Math.abs(cd * Math.pow(cossita[j], 2) / (d2jo/10000) - lights[j]);
//				}
				double d2jo = Math.pow(h, 2) + Math.pow((x-data[k][0]), 2) + Math.pow((y-data[k][1]), 2);
				double dist = Math.abs(cd * Math.pow(cossita, 2) / (d2jo/10000) - data[k][5]);
				
				
				if (dist <= minMoku) {
					minMoku = dist;
					minX = x;
					minY = y;
					minH = h;
				}
			}
//			System.out.println("X:\t" + minX);
//			System.out.println("Y:\t" + minY);
//			System.out.println("H:\t" + minH);
//			System.out.println("min:\t" + minMoku);
//			System.out.println("Dist:\t" + Math.sqrt(Math.pow(minX-data[k][2], 2) + Math.pow(minY-data[k][3], 2) + Math.pow(minH-data[k][4], 2)));
//			System.out.println("DistH:\t" + Math.abs(minH-data[k][4]));
//			System.out.println("PanelX:\t" + (minX/60));
//			System.out.println("PanelY:\t" + (minY/60));
//			for (int j = 0; j < 3; j++) {
//				System.out.println(minH / (Math.sqrt(Math.pow(minH, 2) + Math.pow((minX-area[j][0]), 2) + Math.pow((minY-area[j][1]), 2))));
//			}
			System.out.println(k + "\t" + Math.sqrt(Math.pow(data[k][0]-data[k][2], 2) + Math.pow(data[k][1]-data[k][3], 2) + Math.pow(data[k][4], 2)) 
					+ "\t" + Math.sqrt(Math.pow(minX-data[k][2], 2) + Math.pow(minY-data[k][3], 2) + Math.pow(minH-data[k][4], 2)));
		}
	}

}