/*
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 HCI159　U4　松井健人
 作成開始：7/20

+++++++++++
 このプログラムは
 HCI159での，照度センサの値から天井面までの距離推定を目的を実現するためのものである
 みそは距離推定（逐点法）の際にcos^2をいれることである
+++++++++++
 ※kmatsui_HCI159_配置図.pdf　を参照
 この配置図では右上の照明の位置を原点としている
+++++++++++
 KC111のLED照明を10灯使用している
 使用している照明番号(中島サーバに基づく)は5, 7, 11, 13, 15, 17, 20, 22, 24, 26である
 この番号を順に0~9に割り当てる
 つまり，
0 =  5, 1 =  7, 2 = 11, 3 = 13, 4 = 15, 
5 = 17, 6 = 20, 7 = 22, 8 = 24, 9 = 26
である

light[][]では，
はじめの[]に10灯の照明番号を格納している
次の[]には，
0に照度センサのx座標，1にy座標，2に光度が格納されている
+++++++++++
sensor[][]では，
はじめの[]に6種類の位置を格納している

3つ照度センサの水平面の位置をA, B, C　としている
 また，照度センサの垂直方向の位置は天井から1 m, 2 mの2種類ある
 つまり計6パターンの位置がある
 
sensor[][]では，
はじめの[]に6種類の位置を格納している
つまり，
1 m の照度センサAは　[0]    ，1 m の照度センサBは　[1]    ，1 m の照度センサCは　[2] ，
1.95 m の照度センサAは　[3] ，1.95 m の照度センサBは　[4] ，1.95 m の照度センサCは　[5] ，
である
つぎの[]には0~9までにその位置にある照度センサに対する照明からの照度がそれぞれ格納されており，
10に照度センサのx座標，11にy座標，12に高さが格納されている
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


*/



import java.util.Random;

public class HCI01 {
	
	public static void main(String[] args) {
		new HCI01();
	}
	
	//main文
	public HCI01(){
		
		//light[照明番号][X, Y, 光度]
		double light[][] = {
				{  0,   0, 327},
				{  0, 2.4, 333},
				{1.2, 1.2, 334},
				{1.2, 3.6, 329},
				{2.4,   0, 326},
				{2.4, 2.4, 318},
				{3.6, 1.2, 329},
				{3.6, 3.6, 331},
				{4.8,   0, 327},
				{4.8, 2.4, 338},
		};
		
		//double[1m時のABC，1.95m時のABCの順][照明に対応した照度(0 ~ 9), x, y, z]
		double sensor[][] = {
				//1 m
				{ 6.6, 12.1,   25,   28,  8.5,  328, 27.7, 30.6,  6.7, 13.3, 2.4, 2.4,    1},
				{13.7, 23.2,  128, 20.4, 17.3, 99.5, 19.2,  9.4,  6.2,    7, 1.8, 1.8,    1},
				{11.6, 72.9, 63.3, 71.9,  7.7, 59.4,  8.5,  9.3,  4.2,  5.4, 1.2, 2.4,    1},
				//1.5 m
				{ 9.5, 17.5, 34.6, 37.3, 15.1,  157, 37.1, 40.5, 10.1, 20.1, 2.4, 2.4,  1.5},
				{19.6, 30.9, 94.0, 29.1, 30.1, 89.1, 28.9, 15.2,  9.3, 10.7, 1.8, 1.8,  1.5},
				{16.5, 71.5, 65.2, 71.3, 12.5, 62.0, 13.5, 14.2,  6.0,  7.8, 1.2, 2.4,  1.5},
				//1.95 m
				{11.2,   20, 33.9, 36.8, 18.1,   93, 36.1, 39.2, 11.9, 22.4, 2.4, 2.4, 1.95},
				{22.5, 32.6, 78.5, 29.8, 32.3, 67.3, 28.6, 16.6, 10.8, 12.4, 1.8, 1.8, 1.95},
				{18.9, 60.5, 55.2, 59.9, 14.6, 54.6, 15.1, 16.1,    7,  9.3, 1.2, 2.4, 1.95},
		};
		
		//センサの位置が既知の時で，照明1つから高さ推定
		//positionKnown1light(light, sensor);
		
		//センサの位置が既知の時で，照明3つから高さ推定
		//positionKnown3light(light, sensor);
		
		//センサの位置が未知の時で，照明1つから高さ推定
		//positionUnknown1light(light, sensor);
		
		//センサの位置が未知の時で，照明3つから高さ推定
		//positionUnknown3light(light, sensor);
		
		while(true){
			positionUnknown3light(light, sensor);
			System.out.println();
		}
	}

	//目的関数(既知)
	double ObjectFunction( double light[][], double sensor[][], int lightNum, int sensorNum, double H){
		
		//x軸方向のセンサから照明までの距離
		double X = Math.abs( sensor[ sensorNum ][ 10 ] - light[ lightNum ][ 0 ]);
		//y軸方向のセンサから照明までの距離
		double Y = Math.abs( sensor[ sensorNum ][ 11 ] - light[ lightNum ][ 1 ]);
		
		//推定直線距離
		double L = Math.sqrt( Math.pow( H, 2) + Math.pow( X, 2) + Math.pow( Y, 2) );
		//コサイン　H/L
		double cos = H / L;
		
		//照明の光度
		double E = light[ lightNum ][ 2 ];
		//それぞれの照明からの照度
		double I = sensor[ sensorNum ][ lightNum ];
		
		//目的関数　f = | ( E * cos ^ 2 ) / L ^ 2 - I|
		double Obj = Math.abs( I - E * Math.pow( cos , 1.6 ) / Math.pow( L , 2) );
		
		return Obj;
	}
	
	//目的関数(未知)
	double ObjectFunction( double light[][], double sensor[][], int lightNum, int sensorNum, double H, double X, double Y){
		
		//x軸方向のセンサから照明までの距離
		X = Math.abs( X - light[ lightNum ][ 0 ]);
		//y軸方向のセンサから照明までの距離
		Y = Math.abs( Y - light[ lightNum ][ 1 ]);
		
		//推定直線距離
		double L = Math.sqrt( Math.pow( H, 2) + Math.pow( X, 2) + Math.pow( Y, 2) );
		//コサイン　H/L
		double cos = H / L;
		
		//照明の光度
		double E = light[ lightNum ][ 2 ];
		//それぞれの照明からの照度
		double I = sensor[ sensorNum ][ lightNum ];
		
		//目的関数　f = | ( E * cos ^ 2 ) / L ^ 2 - I|
		double Obj = Math.abs( I - E * Math.pow( cos , 1.6 ) / Math.pow( L , 2) );
		
		return Obj;
	}
	
	//センサの位置が既知の時で，照明1つから高さ推定
	void positionKnown1light(double light[][], double sensor[][]){
		
		//照度センサ番号（1 mのA B C = 0 1 2, 1.95 mのA B C　= 3 4 5 ）
		int sensorNum;
		
		//照明番号（5 ~ 26 = 0 ~ 9 ）
		int lightNum;
		
		for(int a =0 ; a < 9 ; a++ ){
			sensorNum = a;
			
			//センサ情報
			if      ( ( sensorNum % 3 ) == 0) 	System.out.println( " 照度センサA	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			else if ( ( sensorNum % 3 ) == 1) 	System.out.println( " 照度センサB	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			else								System.out.println( " 照度センサC	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			
			//列名
			System.out.println("照明	推定高さ	高さ誤差");
			
			for(int b =0 ; b < 10 ; b++ ){
				lightNum = b; 
				calculateForPositionKnown1light( light, sensor, lightNum, sensorNum  );	
			}
			System.out.println();
		}
	}
	
	//positionKnown1lightの計算
	void calculateForPositionKnown1light( double light[][], double sensor[][], int lightNum, int sensorNum ){
		
		//目的関数の最小値
		double minObj  = Double.MAX_VALUE;
		//推定高さの最小値
		double minH    = Double.MAX_VALUE;
		
		//目的関数が最少となる高さを探す
		for( double i = 0; i < 5; i += 0.00001 ){
			double H = i;
			
			double Obj = ObjectFunction( light, sensor, lightNum, sensorNum, H );
			if(Obj < minObj){
				minObj = Obj;
				minH = H;
			}
		}
		
		//照明と予測高さ
		System.out.printf(lightNum + "	 %.6f	%.6f\n", minH, Math.abs( minH - sensor[ sensorNum ][ 12 ] ) );
	}
	
	//センサの位置が既知の時で，照明3つから高さ推定
	void positionKnown3light(double light[][], double sensor[][]){
		//照度センサ番号（1 mのA B C = 0 1 2, 1.95 mのA B C　= 3 4 5 ）
		int sensorNum;
		
		//照明番号（5 ~ 26 = 0 ~ 9 ）
		int lightNum;
		
		for(int a =0 ; a < 9 ; a++ ){
			sensorNum = a;
			
			//センサ情報
			if      ( ( sensorNum % 3 ) == 0) 	{
				System.out.println( " 照度センサA	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
				calculateForPositionKnown3light( light, sensor, 5, 2, 3, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 5, 2, 6, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 5, 3, 7, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 5, 6, 7, sensorNum  );
			}
			else if ( ( sensorNum % 3 ) == 1) 	{
				System.out.println( " 照度センサB	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
				calculateForPositionKnown3light( light, sensor, 2, 5, 1, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 2, 5, 3, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 2, 5, 4, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 2, 5, 6, sensorNum  );
			}
			else								{
				System.out.println( " 照度センサC	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
				calculateForPositionKnown3light( light, sensor, 1, 2, 3, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 1, 2, 5, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 1, 3, 5, sensorNum  );
				calculateForPositionKnown3light( light, sensor, 2, 3, 5, sensorNum  );
			}
			
			System.out.println();
		}
	}
	
	//positionKnown3lightの計算
	void calculateForPositionKnown3light( double light[][], double sensor[][], int lightNum1, int lightNum2, int lightNum3, int sensorNum ){
		
		//目的関数の最小値
		double minObj  = Double.MAX_VALUE;
		//推定高さの最小値
		double minH    = Double.MAX_VALUE;
		
		//目的関数が最少となる高さを探す
		for( double i = 0; i < 5; i += 0.001 ){
			double H = i;
			
			double Obj1 = ObjectFunction( light, sensor, lightNum1, sensorNum, H );
			double Obj2 = ObjectFunction( light, sensor, lightNum2, sensorNum, H );
			double Obj3 = ObjectFunction( light, sensor, lightNum3, sensorNum, H );
			double Obj  = Obj1 + Obj2 + Obj3;
			
			if(Obj < minObj){
				minObj = Obj;
				minH = H;
			}
			
		}
		
		//照明と予測高さ
		System.out.printf( " 照明 " + lightNum1 + ", " + lightNum2 + ", " + lightNum3 + "	推定 %.6f m 	誤差 %.6f m \n", minH, Math.abs( minH - sensor[ sensorNum ][ 12 ] ) );
		
	}
	
	//センサの位置が未知の時で，照明1つから高さ推定
	void positionUnknown1light(double light[][], double sensor[][]){
		//照度センサ番号（1 mのA B C = 0 1 2, 1.95 mのA B C　= 3 4 5 ）
		int sensorNum;
		
		//照明番号（5 ~ 26 = 0 ~ 9 ）
		int lightNum;
		
		for(int a =0 ; a < 9 ; a++ ){
			sensorNum = a;
			
			//センサ情報
			if      ( ( sensorNum % 3 ) == 0) 	System.out.println( " 照度センサA	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			else if ( ( sensorNum % 3 ) == 1) 	System.out.println( " 照度センサB	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			else								System.out.println( " 照度センサC	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			
			for(int b =0 ; b < 10 ; b++ ){
				lightNum = b; 
				calculateForPositionUnknown1light( light, sensor, lightNum, sensorNum  );	
			}
			System.out.println();
		}
	}
	
	//positionUnknown1lightの計算
	void calculateForPositionUnknown1light( double light[][], double sensor[][], int lightNum, int sensorNum ){
		
		//目的関数の最小値
		double minObj  = Double.MAX_VALUE;
		//推定高さの最小値
		double minH    = Double.MAX_VALUE;
		
		//乱数
		Random rand = new Random();
		
		//目的関数が最少となる高さを探す
		for( double i = 0; i < 1000000; i++ ){
			//x軸方向　0 ~ 4.8 m
			double X = rand.nextDouble() * 4.8;
			//y軸方向　0 ~ 3.6 m
			double Y = rand.nextDouble() * 3.6;
			//h軸方向　0 ~ 5.0 m
			double H = rand.nextDouble() * 5.0;
			
			double Obj = ObjectFunction( light, sensor, lightNum, sensorNum, H, X, Y );
			if(Obj < minObj){
				minObj = Obj;
				minH = H;
			}
		}
		
		//照明と予測高さ
		System.out.printf( " 照明 " + lightNum + "	推定 %.6f m 	誤差 %.6f m \n", minH, Math.abs( minH - sensor[ sensorNum ][ 12 ] ) );
	}
	
	//センサの位置が未知の時で，照明3つから高さ推定
	void positionUnknown3light(double light[][], double sensor[][]){
		//照度センサ番号（1 mのA B C = 0 1 2, 1.95 mのA B C　= 3 4 5 ）
		int sensorNum;
		
		//照明番号（5 ~ 26 = 0 ~ 9 ）
		int lightNum;
		
		//for(int a =0 ; a < 6 ; a++ ){
		for(int a =0 ; a < 9 ; a++ ){
			sensorNum = a;
			
			//センサ情報
			if      ( ( sensorNum % 3 ) == 0) 	{
				//System.out.println( " 照度センサA	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
				
				//列名
				//System.out.println("照明1	照明2	照明3	推定高さ	高さ誤差	X	Y");
				
				calculateForPositionUnknown3light( light, sensor, 5, 2, 3, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 5, 2, 6, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 5, 3, 7, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 5, 6, 7, sensorNum  );
			}
			else if ( ( sensorNum % 3 ) == 1) 	{
				//System.out.println( " 照度センサB	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
				
				//列名
				//System.out.println("照明1	照明2	照明3	推定高さ	高さ誤差	X	Y");
				
				calculateForPositionUnknown3light( light, sensor, 2, 5, 1, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 2, 5, 3, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 2, 5, 4, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 2, 5, 6, sensorNum  );
			}
			else								{
				//System.out.println( " 照度センサC	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
				
				//列名
				//System.out.println("照明1	照明2	照明3	推定高さ	高さ誤差	X	Y");
				
				calculateForPositionUnknown3light( light, sensor, 1, 2, 3, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 1, 2, 5, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 1, 3, 5, sensorNum  );
				calculateForPositionUnknown3light( light, sensor, 2, 3, 5, sensorNum  );
			}
			System.out.print("	");
//			System.out.println();
			
			
		}
	}
	
	//positionUnknown3lightの計算
	void calculateForPositionUnknown3light( double light[][], double sensor[][], int lightNum1, int lightNum2, int lightNum3, int sensorNum ){
		
		//目的関数の最小値
		double minObj  = Double.MAX_VALUE;
		//推定高さの最小値
		double minH    = Double.MAX_VALUE;
		
		//目的関数の最小値のときのセンサのx座標
		double ObjX    = Double.MAX_VALUE;
		//目的関数の最小値のときのセンサのy座標
		double ObjY    = Double.MAX_VALUE;
		
		//目的関数の最小値のときの照明1の目的関数の値
		double minObj1    = Double.MAX_VALUE;
		//目的関数の最小値のときの照明2の目的関数の値
		double minObj2    = Double.MAX_VALUE;
		//目的関数の最小値のときの照明3の目的関数の値
		double minObj3    = Double.MAX_VALUE;
		
		
		//乱数
		Random rand = new Random();
		
		//目的関数が最少となる高さを探す
		for( double i = 0; i < 1.0E8; i++ ){
//			//x軸方向　0 ~ 4.8 m
//			double X = rand.nextDouble() * 4.8;
//			//y軸方向　0 ~ 3.6 m
//			double Y = rand.nextDouble() * 3.6;
			
			//x軸方向　0 ~ 4.8 m
			double X = rand.nextDouble() * 2.0 + sensor[ sensorNum ][ 10 ] - 1.0;
			//y軸方向　0 ~ 3.6 m
			double Y = rand.nextDouble() * 2.0 + sensor[ sensorNum ][ 11 ] - 1.0;
			
			//h軸方向　0 ~ 5.0 m
			//double H = rand.nextDouble() * 5.0;
			//h軸方向　0 ~ 5.0 m
			double H = rand.nextDouble() * 2.0 + sensor[ sensorNum ][ 12 ] - 1.0;
			
			double Obj1 = ObjectFunction( light, sensor, lightNum1, sensorNum, H , X, Y );
			double Obj2 = ObjectFunction( light, sensor, lightNum2, sensorNum, H , X, Y );
			double Obj3 = ObjectFunction( light, sensor, lightNum3, sensorNum, H , X, Y );
			double Obj  = Obj1 + Obj2 + Obj3;
			
			if(Obj < minObj){
				minObj = Obj;
				minH = H;
				ObjX = X;
				ObjY = Y;
				minObj1 = Obj1;
				minObj2 = Obj2;
				minObj3 = Obj3;
			}
			
		}
		
		//照明と予測高さ
		//System.out.printf( lightNum1 + "	" + lightNum2 + "	" + lightNum3 + "	 %.8f	%.8f	"+ ObjX +"	"+ ObjY +"\n", minH, Math.abs( minH - sensor[ sensorNum ][ 12 ] ) );
		System.out.printf( "%.8f	",Math.abs( minH - sensor[ sensorNum ][ 12 ] ) );
		double lE = Math.pow( ( ObjX - sensor[ sensorNum ][ 10 ] ), 2) + Math.pow( ( ObjY - sensor[ sensorNum ][ 11 ] ), 2);
		//System.out.printf( "%.8f	",Math.sqrt( lE ) );
		
		System.out.printf( "%.8f	",Math.abs( ObjX - sensor[ sensorNum ][ 10 ] ) );
		System.out.printf( "%.8f	",Math.abs( ObjY - sensor[ sensorNum ][ 11 ] ) );
		
	}
}
