/*
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 HCI159　U4　松井健人
 作成開始：7/20

+++++++++++
 このプログラムは
 HCI159での，照度センサの値から天井面までの距離推定を目的を実現するために作成した
 HCI01.javaの検算用プログラムである
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/


import java.util.Random;

public class HCI02 {
	public static void main(String[] args) {
		new HCI02();
	}
	
	//main文
	public HCI02(){
		
		//light[照明番号][X, Y, 光度]
		double light[][] = {
				{  0,   0, 327},
				{  0, 2.4, 333},
				{1.2, 1.2, 334},
				{1.2, 3.6, 329},
				{2.4,   0, 326},
				{2.4, 2.4, 318},
				{3.6, 1.2, 329},
				{3.6, 3.6, 331},
				{4.8,   0, 327},
				{4.8, 2.4, 338},
		};
		
		//double[1m時のABC，1.95m時のABCの順][照明に対応した照度(0 ~ 9), x, y, z]
		double sensor[][] = {
				{ 6.6, 12.1,   25,   28,  8.5,  328, 27.7, 30.6,  6.7, 13.3, 2.4, 2.4,    1},
				{13.7, 23.2,  128, 20.4, 17.3, 99.5, 19.2,  9.4,  6.2,    7, 1.8, 1.8,    1},
				{11.6, 72.9, 63.3, 71.9,  7.7, 59.4,  8.5,  9.3,  4.2,  5.4, 1.2, 2.4,    1},
				{11.2,   20, 33.9, 36.8, 18.1,   93, 36.1, 39.2, 11.9, 22.4, 2.4, 2.4, 1.95},
				{22.5, 32.6, 78.5, 29.8, 32.3, 67.3, 28.6, 16.6, 10.8, 12.4, 1.8, 1.8, 1.95},
				{18.9, 60.5, 55.2, 59.9, 14.6, 54.6, 15.1, 16.1,    7,  9.3, 1.2, 2.4, 1.95},
		};
		
		
		//checksForHeight(light, sensor, 6, 2, 2.4);
		luxEstandAct(light, sensor);
	}
	
	
	//実照度と理論照度
	void luxEstandAct(double light[][], double sensor[][]){
		//照度センサ番号（1 mのA B C = 0 1 2, 1.95 mのA B C　= 3 4 5 ）
		int sensorNum;
		//照明番号（5 ~ 26 = 0 ~ 9 ）
		int lightNum;
		
		for(int a =0 ; a < 6 ; a++ ){
			sensorNum = a;
			
			//センサ情報
			if      ( ( sensorNum % 3 ) == 0) 	System.out.println( " 照度センサA	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			else if ( ( sensorNum % 3 ) == 1) 	System.out.println( " 照度センサB	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			else								System.out.println( " 照度センサC	高さ：" + sensor[ sensorNum ][ 12 ] + " m" );
			
			for(int b =0 ; b < 10 ; b++ ){
				lightNum = b;
				
				double W = Math.sqrt( Math.pow((sensor[ sensorNum ][ 10 ] - light[ lightNum ][ 0 ]), 2) + Math.pow((sensor[ sensorNum ][ 11 ] - light[ lightNum ][ 1 ]), 2) ); 
				double H = sensor[ sensorNum ][ 12 ];
				double L = Math.sqrt( W * W + H * H);
				double cos = H / L;
				
				double lux = light[ lightNum ][ 2 ] * Math.pow( cos, 2 ) / Math.pow( L, 2 );
				
				System.out.printf("%.2f	%.6f\n",sensor[ sensorNum ][ lightNum ], lux);
				
				
				
			}
		}
		
	}
	
	//検算プログラム（照明1灯）
	void checksForHeight( double light[][], double sensor[][], int lightNum, int sensorNum, double H ){
	
		System.out.println(lightNum+"	"+sensorNum);
		for(double i = -2; i < 2; i += 0.01){
			
			double obj = ObjectFunction(light, sensor, lightNum, sensorNum, (H+i) );
			//System.out.printf("%.4f	%.6f\n",(H+i) ,obj);
		}
		System.out.println();
		
	}
	
	//検算プログラム（照明3灯）
	void checksForHeight( double light[][], double sensor[][], int lightNum1, int lightNum2, int lightNum3, int sensorNum, double H ){
	
	}
	
	//目的関数(既知)
	double ObjectFunction( double light[][], double sensor[][], int lightNum, int sensorNum, double H){
		
		//x軸方向のセンサから照明までの距離
		double X = Math.abs( sensor[ sensorNum ][ 10 ] - light[ lightNum ][ 0 ]);
		//y軸方向のセンサから照明までの距離
		double Y = Math.abs( sensor[ sensorNum ][ 11 ] - light[ lightNum ][ 1 ]);
		
		//推定直線距離
		double L = Math.sqrt( Math.pow( H, 2) + Math.pow( X, 2) + Math.pow( Y, 2) );
		//コサイン　H/L
		double cos = H / L;
		
		//照明の光度
		double E = light[ lightNum ][ 2 ];
		//それぞれの照明からの照度
		double I = sensor[ sensorNum ][ lightNum ];
		
		//目的関数　f = | ( E * cos ^ 2 ) / L ^ 2 - I|
		double Obj = Math.abs( I - E * Math.pow( cos , 1 ) / Math.pow( L , 2) );
		System.out.printf("%.2f	%.2f	%.6f\n", H, I,(E * Math.pow( cos , 2 ) / Math.pow( L , 2)));
		return Obj;
	}
	
	//目的関数(未知)
	double ObjectFunction( double light[][], double sensor[][], int lightNum, int sensorNum, double H, double X, double Y){
		
		//x軸方向のセンサから照明までの距離
		X = Math.abs( X - light[ lightNum ][ 0 ]);
		//y軸方向のセンサから照明までの距離
		Y = Math.abs( Y - light[ lightNum ][ 1 ]);
		
		//推定直線距離
		double L = Math.sqrt( Math.pow( H, 2) + Math.pow( X, 2) + Math.pow( Y, 2) );
		//コサイン　H/L
		double cos = H / L;
		
		//照明の光度
		double E = light[ lightNum ][ 2 ];
		//それぞれの照明からの照度
		double I = sensor[ sensorNum ][ lightNum ];
		
		//目的関数　f = | ( E * cos ^ 2 ) / L ^ 2 - I|
		double Obj = Math.abs( I - E * Math.pow( cos , 2 ) / Math.pow( L , 2) );
		
		return Obj;
	}
}
